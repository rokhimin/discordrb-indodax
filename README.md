# DiscordBot Ruby Indodax

### About
bot discord for check price crypto coins | API Indodax

### Info
###### Language :
Ruby
###### Library or dependencies :
Discordrb
###### API :
[API indodax](https://indodax.com/downloads/BITCOINCOID-API-DOCUMENTATION.pdf)

### Run
- ```bundle install```
- ```ruby linkstart.rb```

### Deploy
[Heroku](https://github.com/rokhimin/discordrb-heroku)

### Screenshots
- ![](https://i.imgur.com/JJiq4TO.jpg)
- ![](https://i.imgur.com/Mpcfh17.jpg)


